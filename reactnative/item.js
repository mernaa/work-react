import React, { Component } from 'react';
import { Image } from 'react-native';
import { Header, Button, Card, CardItem, Title, Text, H1, Icon, Left, Body, Right, List, View } from 'native-base';

import Modal from "react-native-modal";

import Meteor, { withTracker } from 'react-native-meteor';

import Item from './../invite/item'

import img from './../../../assets/event.jpg'
import history from './../../../routes/history'
import moment from 'moment';



class SubEventItem extends Component {

    state = {
        event: undefined,
        active: false,
        going: false,
        ignore: false,
        text: '',
        isModalVisible: false,
        invited: [],
    }
    // change state of modal
    _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });

    // invite to the user using the id of user
    addToInvite = (_id) => {
        let newInvited = [...this.state.invited, _id]
        this.setState({ invited: newInvited })
    }

    renderPost = () => {
        // map to all users to add invite to subevent
        return this.props.userlist.map((item, index) => {
            // let username = this.props.userlist.find(d=>d._id === item.userId).username ;
            return <Item {...item} props={{ username: item.username }} addInvite={this.addToInvite} key={index} />
        })
    }

    render() {
        const {
            username,
            profile: { country }
        } = Meteor.user()

        const style = {
            margin: 10,
            padding: 25
        }

        //one item of subEvent
        return (

            <Card >
                <CardItem>
                    <H1 style={{ color: 'blue' }}>{this.props.EventName}</H1>
                </CardItem>

                <CardItem>
                    <Icon name='color-filter' />
                    <Text>{this.props.details}</Text>
                </CardItem>

                <CardItem>
                    <Icon type="FontAwesome" name="money" />
                    <Text>{this.props.fees}</Text>
                </CardItem>
                {/* invite freind to sub event */}
                <CardItem>
                    <Button vertical transparent onPress={this._toggleModal} >
                        <Icon type="FontAwesome" name="envelope" style={{ color: 'black' }} />
                        <Text style={{ color: 'black' }} onPress={this._toggleModal}>Invite</Text>
                        {/* Modal that show the users to send them the invite */}
                        <Modal isVisible={this.state.isModalVisible}>
                            <Header>
                                <Left>
                                    <Button transparent onPress={this._toggleModal}>
                                        <Icon name='arrow-back' />
                                    </Button>
                                </Left>
                                <Body>
                                    <Title>Invite</Title>
                                </Body>
                                <Right>
                                    <Button transparent style={style} onPress={() => this.renderInvite()}>
                                        <Text>Send</Text>
                                    </Button>
                                </Right>
                            </Header>
                            <Card>
                                <List>
                                    {this.renderPost()}
                                </List>
                            </Card>
                        </Modal>
                    </Button>
                </CardItem>
            </Card>

        );
    }
}

export default withTracker(params => {
    const handle = Meteor.subscribe('event');

    return {
        todosReady: handle.ready(),
        userlist: Meteor.collection('users').find(),

    };

})(SubEventItem);
