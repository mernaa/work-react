import React, { Component } from 'react'
import classes from './style.less'
import { map, get, find } from 'lodash'
import { connect } from 'react-redux';
import mapDispatchToProps from 'helpers/actions/main'
class Header extends Component {
    constructor(props) {
        super(props)
    }

    renderTrs = () => {
        const { businessDays, renderTime } = this.props
        // body of table report that I want to show 
        //renderTime that use the time with momentjs to be like that Apr 10, 2019 12:44 PM
        const trs = {
            Location: {
                key: "Location",
                value: "ABC"
            },
            BusinessDay: {
                key: "Business Day",
                value: `${businessDays && businessDays.business_day}`
            },
            Started: {
                key: "",
                value: `Started @ ${renderTime(businessDays && businessDays.start_time)}`
            },
            Ended: {
                key: "",
                value: `${businessDays && businessDays.end_time == null ? '' : `Ended @ ${renderTime(businessDays && businessDays.end_time)}`}`
            },
            FirstOrder: {
                key: "First Order",
                value: "dd/mm/yyyy hh:mm:ss"
            },
            LastOrder: {
                key: "Last Order",
                value: "dd/mm/yyyy hh:mm:ss"
            },
            Receipts: {
                key: "Receipts",
                value: "Count # (From Serial # To Serial #)"
            },
            Currency: {
                key: "Currency",
                value: "EGP"
            }
        }

        // map the body to show all the content by key and value
        return map(trs, (d, key) => (
            <tr>
                <td className={classes.key}>{d.key}</td>
                <td className={classes.value}>{d.value}</td>
            </tr>
        ))
    }
    render() {
        return (
            <>
                <div className={classes.details}>
                    <table>
                        <tbody className={classes.tbody_details}>
                            {this.renderTrs()}
                        </tbody>
                    </table>
                </div>
            </>
        )
    }
}
const mapStateToProps = (state, props) => ({
    selected: get(state.businessDays, 'selected', ''),
    active: get(state.businessDays, 'active', '') || {},
    get businessDays() { return find(state.businessDays.data, { id: this.selected || this.active }) },
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)
